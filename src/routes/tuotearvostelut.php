<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

// Hae kaikki tuotearvostelut
$app->get('/api/tuotearvostelut', function(Request $request, Response $response ){
   $sql = "SELECT * FROM tuotearvostelut";
   
   try{
        // Hae db objekti
        $db = new db();
        //Connect
        $db = $db->connect();
        
        $stmt = $db->query($sql);
        $tuotearvostelut = $stmt->fetchAll();
        $db = null;
        echo json_encode($tuotearvostelut);
   } catch(PDOException $e){
       echo '{"error":{"text: '.$e->getMessage().'}';
   }
   
});

// Hae yksittäinen tuotearvostelu
$app->get('/api/tuotearvostelu/{id}', function(Request $request, Response $response ){
    $id = $request->getAttribute('id');    
    
   $sql = "SELECT * FROM tuotearvostelut WHERE id = $id";
   
   try{
        // Hae db objekti
        $db = new db();
        //Connect
        $db = $db->connect();
        
        $stmt = $db->query($sql);
        $tuotearvostelu = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($tuotearvostelu);
   } catch(PDOException $e){
       echo '{"error":{"text: '.$e->getMessage().'}';
   }
   
});

// Lisää tuotearvostelu
$app->post('/api/tuotearvostelu/lisaa', function(Request $request, Response $response ){
    $tuotetahdet = $request->getParam('tuotetahdet');
    $tuotteenarvostelija = $request->getParam('tuotteenarvostelija');
    $tuotearvostelu = $request->getParam('tuotearvostelu');

    
    $sql = "INSERT INTO tuotearvostelut(tuotetahdet,tuotteenarvostelija,tuotearvostelu) VALUES(:tuotetahdet,:tuotteenarvostelija,:tuotearvostelu)";
   
   try{
        // Hae DB objekti
        $db = new db();
        //Connect
        $db = $db->connect();
        
        $stmt = $db->prepare($sql);
        
        $stmt->bindParam(':tuotetahdet', $tuotetahdet);
        $stmt->bindParam(':tuotteenarvostelija', $tuotteenarvostelija);
        $stmt->bindParam(':tuotearvostelu', $tuotearvostelu);
        
        $stmt->execute();
        
        echo '{"notice: {"text": "Tuotearvostelu lisätty"}';
        
   } catch(PDOException $e){
       echo '{"error":{"text: '.$e->getMessage().'}';
   }
   
});

// Päivitä tuotearvostelua
$app->put('/api/tuotearvostelu/paivita/{id}', function(Request $request, Response $response ){
    $id = $request->getAttribute('id');
    $tuotetahdet = $request->getParam('tuotetahdet');
    $tuotteenarvostelija = $request->getParam('tuotteenarvostelija');
    $tuotearvostelu = $request->getParam('tuotearvostelu');

    $sql = "UPDATE tuotearvostelut SET
                tuotetahdet = :tuotetahdet,
                tuotteenarvostelija = :tuotteenarvostelija,
                tuotearvostelu = :tuotearvostelu
            WHERE id = $id";
   
   try{
        // Hae DB objekti
        $db = new db();
        //Connect
        $db = $db->connect();
        
        $stmt = $db->prepare($sql);
        
        $stmt->bindParam(':tuotetahdet', $tuotetahdet);
        $stmt->bindParam(':tuotteenarvostelija', $tuotteenarvostelija);
        $stmt->bindParam(':tuotearvostelu', $tuotearvostelu);

        
        $stmt->execute();
        
        echo '{"notice: {"text": "Tuotearvostelu päivitetty"}';
        
   } catch(PDOException $e){
       echo '{"error":{"text: '.$e->getMessage().'}';
   }
   
});

// Poista yksittäinen tuotearvostelu
$app->delete('/api/tuotearvostelu/poista/{id}', function(Request $request, Response $response ){
    $id = $request->getAttribute('id');    
    
   $sql = "DELETE FROM tuotearvostelut WHERE id = $id";
   
   try{
        // Hae db objekti
        $db = new db();
        //Connect
        $db = $db->connect();
        
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $db = null;
        echo '{"notice: {"text": "Tuotearvostelu poistettu"}';
   } catch(PDOException $e){
       echo '{"error":{"text: '.$e->getMessage().'}';
   }
   
});
