<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

// Hae kaikki tuotteet
$app->get('/api/tuotteet', function(Request $request, Response $response ){
   $sql = "SELECT * FROM tuotteet";
   
   try{
        // Hae db objekti
        $db = new db();
        //Connect
        $db = $db->connect();
        
        $stmt = $db->query($sql);
        $tuotteet = $stmt->fetchAll();
        $db = null;
        echo json_encode($tuotteet);
   } catch(PDOException $e){
       echo '{"error":{"text: '.$e->getMessage().'}';
   }
   
});

// Hae yksittäinen tuote
$app->get('/api/tuote/{id}', function(Request $request, Response $response ){
    $id = $request->getAttribute('id');    
    
   $sql = "SELECT * FROM tuotteet WHERE id = $id";
   
   try{
        // Hae db objekti
        $db = new db();
        //Connect
        $db = $db->connect();
        
        $stmt = $db->query($sql);
        $tuote = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($tuote);
   } catch(PDOException $e){
       echo '{"error":{"text: '.$e->getMessage().'}';
   }
   
});

// Lisää tuote
$app->post('/api/tuote/lisaa', function(Request $request, Response $response ){
    $tuotenimi = $request->getParam('tuotenimi');
    $tuotehinta = $request->getParam('tuotehinta');
    $tuoteesittely = $request->getParam('tuoteesittely');

    
    $sql = "INSERT INTO tuotteet(tuotenimi,tuotehinta,tuoteesittely) VALUES(:tuotenimi,:tuotehinta,:tuoteesittely)";
   
   try{
        // Hae DB objekti
        $db = new db();
        //Connect
        $db = $db->connect();
        
        $stmt = $db->prepare($sql);
        
        $stmt->bindParam(':tuotenimi', $tuotenimi);
        $stmt->bindParam(':tuotehinta', $tuotehinta);
        $stmt->bindParam(':tuoteesittely', $tuoteesittely);
        
        $stmt->execute();
        
        echo '{"notice: {"text": "Tuote lisätty"}';
        
   } catch(PDOException $e){
       echo '{"error":{"text: '.$e->getMessage().'}';
   }
   
});

// Päivitä tuotetta
$app->put('/api/tuote/paivita/{id}', function(Request $request, Response $response ){
    $id = $request->getAttribute('id');
    $tuotenimi = $request->getParam('tuotenimi');
    $tuotehinta = $request->getParam('tuotehinta');
    $tuoteesittely = $request->getParam('tuoteesittely');

    $sql = "UPDATE asiakkaat SET
                tuotenimi = :tuotenimi,
                tuotehinta = :tuotehinta,
                tuoteesittely = :tuoteesittely
            WHERE id = $id";
   
   try{
        // Hae DB objekti
        $db = new db();
        //Connect
        $db = $db->connect();
        
        $stmt = $db->prepare($sql);
        
        $stmt->bindParam(':tuotenimi', $tuotenimi);
        $stmt->bindParam(':tuotehinta', $tuotehinta);
        $stmt->bindParam(':tuoteesittely', $tuoteesittely);

        
        $stmt->execute();
        
        echo '{"notice: {"text": "Tuote päivitetty"}';
        
   } catch(PDOException $e){
       echo '{"error":{"text: '.$e->getMessage().'}';
   }
   
});

// Poista yksittäinen tuote
$app->delete('/api/tuote/poista/{id}', function(Request $request, Response $response ){
    $id = $request->getAttribute('id');    
    
   $sql = "DELETE FROM tuotteet WHERE id = $id";
   
   try{
        // Hae db objekti
        $db = new db();
        //Connect
        $db = $db->connect();
        
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $db = null;
        echo '{"notice: {"text": "Tuote poistettu"}';
   } catch(PDOException $e){
       echo '{"error":{"text: '.$e->getMessage().'}';
   }
   
});
