<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

// Hae kaikki asiakkaat
$app->get('/api/asiakkaat', function(Request $request, Response $response ){
   $sql = "SELECT * FROM asiakkaat";
   
   try{
        // Hae db objekti
        $db = new db();
        //Connect
        $db = $db->connect();
        
        $stmt = $db->query($sql);
        $asiakkaat = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($asiakkaat);
   } catch(PDOException $e){
       echo '{"error":{"text: '.$e->getMessage().'}';
   }
   
});

// Hae yksittäinen asiakas
$app->get('/api/asiakas/{id}', function(Request $request, Response $response ){
    $id = $request->getAttribute('id');    
    
   $sql = "SELECT * FROM asiakkaat WHERE id = $id";
   
   try{
        // Hae db objekti
        $db = new db();
        //Connect
        $db = $db->connect();
        
        $stmt = $db->query($sql);
        $asiakas = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($asiakas);
   } catch(PDOException $e){
       echo '{"error":{"text: '.$e->getMessage().'}';
   }
   
});

// Lisää aiaskas
$app->post('/api/asiakas/lisaa', function(Request $request, Response $response ){
    $etunimi = $request->getParam('etunimi');
    $sukunimi = $request->getParam('sukunimi');
    $puhelinnumero = $request->getParam('puhelinnumero');
    $sahkoposti = $request->getParam('sahkoposti');
    $kotiosoite = $request->getParam('kotiosoite');
    $kaupunki = $request->getParam('kaupunki');
    $postinumero = $request->getParam('postinumero');
    $kayttajanimi = $request->getParam('kayttajanimi');
    
    $sql = "INSERT INTO asiakkaat(etunimi,sukunimi,puhelinnumero,sahkoposti,kotiosoite,
    kaupunki,postinumero, kayttajanimi) VALUES(:etunimi,:sukunimi,:puhelinnumero,:sahkoposti,:kotiosoite,:kaupunki,:postinumero, :kayttajanimi)";
   
   try{
        // Hae DB objekti
        $db = new db();
        //Connect
        $db = $db->connect();
        
        $stmt = $db->prepare($sql);
        
        $stmt->bindParam(':etunimi', $etunimi);
        $stmt->bindParam(':sukunimi', $sukunimi);
        $stmt->bindParam(':puhelinnumero', $puhelinnumero);
        $stmt->bindParam(':sahkoposti', $sahkoposti);
        $stmt->bindParam(':kotiosoite', $kotiosoite);
        $stmt->bindParam(':kaupunki', $kaupunki);
        $stmt->bindParam(':postinumero', $postinumero);
        $stmt->bindParam(':kayttajanimi', $kayttajanimi);
        
        $stmt->execute();
        
        echo '{"notice: {"text": "Asiakas lisätty"}';
        
   } catch(PDOException $e){
       echo '{"error":{"text: '.$e->getMessage().'}';
   }
   
});

// Päivitä aiaskasta
$app->put('/api/asiakas/paivita/{id}', function(Request $request, Response $response ){
    $id = $request->getAttribute('id');
    $etunimi = $request->getParam('etunimi');
    $sukunimi = $request->getParam('sukunimi');
    $puhelinnumero = $request->getParam('puhelinnumero');
    $sahkoposti = $request->getParam('sahkoposti');
    $kotiosoite = $request->getParam('kotiosoite');
    $kaupunki = $request->getParam('kaupunki');
    $postinumero = $request->getParam('postinumero');
    $kayttajanimi = $request->getParam('kayttajanimi');
    
    $sql = "UPDATE asiakkaat SET
                etunimi = :etunimi,
                sukunimi = :sukunimi,
                puhelinnumero = :puhelinnumero,
                sahkoposti = :sahkoposti,
                kotiosoite = :kotiosoite,
                kaupunki = :kaupunki,
                postinumero = :postinumero,
                kayttajanimi = :kayttajanimi
            WHERE id = $id";
   
   try{
        // Hae DB objekti
        $db = new db();
        //Connect
        $db = $db->connect();
        
        $stmt = $db->prepare($sql);
        
        $stmt->bindParam(':etunimi', $etunimi);
        $stmt->bindParam(':sukunimi', $sukunimi);
        $stmt->bindParam(':puhelinnumero', $puhelinnumero);
        $stmt->bindParam(':sahkoposti', $sahkoposti);
        $stmt->bindParam(':kotiosoite', $kotiosoite);
        $stmt->bindParam(':kaupunki', $kaupunki);
        $stmt->bindParam(':postinumero', $postinumero);
        $stmt->bindParam(':kayttajanimi', $kayttajanimi);
        
        $stmt->execute();
        
        echo '{"notice: {"text": "Asiakas päivitetty"}';
        
   } catch(PDOException $e){
       echo '{"error":{"text: '.$e->getMessage().'}';
   }
   
});

// Poista yksittäinen asiakas
$app->delete('/api/asiakas/poista/{id}', function(Request $request, Response $response ){
    $id = $request->getAttribute('id');    
    
   $sql = "DELETE FROM asiakkaat WHERE id = $id";
   
   try{
        // Hae db objekti
        $db = new db();
        //Connect
        $db = $db->connect();
        
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $db = null;
        echo '{"notice: {"text": "Asiakas poistettu"}';
   } catch(PDOException $e){
       echo '{"error":{"text: '.$e->getMessage().'}';
   }
   
});


#### TUOTTEET ############################################################################################






############# TUOTEARVOSTELUT #####################################################################


// Hae kaikki tuotteet
$app->get('/api/tuotteet', function(Request $request, Response $response ){
   $sql = "SELECT * FROM tuotteet";
   
   try{
        // Hae db objekti
        $db = new db();
        //Connect
        $db = $db->connect();
        
        $stmt = $db->query($sql);
        $tuotteet = $stmt->fetchAll();
        $db = null;
        echo json_encode($tuotteet);
   } catch(PDOException $e){
       echo '{"error":{"text: '.$e->getMessage().'}';
   }
   
});

// Hae yksittäinen tuote
$app->get('/api/tuote/{id}', function(Request $request, Response $response ){
    $id = $request->getAttribute('id');    
    
   $sql = "SELECT * FROM tuotteet WHERE id = $id";
   
   try{
        // Hae db objekti
        $db = new db();
        //Connect
        $db = $db->connect();
        
        $stmt = $db->query($sql);
        $tuote = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($tuote);
   } catch(PDOException $e){
       echo '{"error":{"text: '.$e->getMessage().'}';
   }
   
});

// Lisää tuote
$app->post('/api/tuote/lisaa', function(Request $request, Response $response ){
    $tuotenimi = $request->getParam('tuotenimi');
    $tuotehinta = $request->getParam('tuotehinta');
    $tuoteesittely = $request->getParam('tuoteesittely');

    
    $sql = "INSERT INTO tuotteet(tuotenimi,tuotehinta,tuoteesittely) VALUES(:tuotenimi,:tuotehinta,:tuoteesittely)";
   
   try{
        // Hae DB objekti
        $db = new db();
        //Connect
        $db = $db->connect();
        
        $stmt = $db->prepare($sql);
        
        $stmt->bindParam(':tuotenimi', $tuotenimi);
        $stmt->bindParam(':tuotehinta', $tuotehinta);
        $stmt->bindParam(':tuoteesittely', $tuoteesittely);
        
        $stmt->execute();
        
        echo '{"notice: {"text": "Tuote lisätty"}';
        
   } catch(PDOException $e){
       echo '{"error":{"text: '.$e->getMessage().'}';
   }
   
});

// Päivitä tuotetta
$app->put('/api/tuote/paivita/{id}', function(Request $request, Response $response ){
    $id = $request->getAttribute('id');
    $tuotenimi = $request->getParam('tuotenimi');
    $tuotehinta = $request->getParam('tuotehinta');
    $tuoteesittely = $request->getParam('tuoteesittely');

    $sql = "UPDATE asiakkaat SET
                tuotenimi = :tuotenimi,
                tuotehinta = :tuotehinta,
                tuoteesittely = :tuoteesittely
            WHERE id = $id";
   
   try{
        // Hae DB objekti
        $db = new db();
        //Connect
        $db = $db->connect();
        
        $stmt = $db->prepare($sql);
        
        $stmt->bindParam(':tuotenimi', $tuotenimi);
        $stmt->bindParam(':tuotehinta', $tuotehinta);
        $stmt->bindParam(':tuoteesittely', $tuoteesittely);

        
        $stmt->execute();
        
        echo '{"notice: {"text": "Tuote päivitetty"}';
        
   } catch(PDOException $e){
       echo '{"error":{"text: '.$e->getMessage().'}';
   }
   
});

// Poista yksittäinen tuote
$app->delete('/api/tuote/poista/{id}', function(Request $request, Response $response ){
    $id = $request->getAttribute('id');    
    
   $sql = "DELETE FROM tuotteet WHERE id = $id";
   
   try{
        // Hae db objekti
        $db = new db();
        //Connect
        $db = $db->connect();
        
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $db = null;
        echo '{"notice: {"text": "Tuote poistettu"}';
   } catch(PDOException $e){
       echo '{"error":{"text: '.$e->getMessage().'}';
   }
   
});