window.onload = function(){
    /*
    * Tarkistetaan, että lomakkeessa ei ole tyhjiä kohtia
    */
    function tarkistaLomake(){
        var etunimi = document.forms["rekLomake"]["etunimi"].value;
        var sukunimi = document.forms["rekLomake"]["sukunimi"].value;
        var puhelinnumero = document.forms["rekLomake"]["puhelinnumero"].value;
        var kotiosoite = document.forms["rekLomake"]["kotiosoite"].value;
        var postinumero = document.forms["rekLomake"]["postinumero"].value;
        var kaupunki = document.forms["rekLomake"]["kaupunki"].value;
        var sahkoposti = document.forms["rekLomake"]["sahkoposti"].value;
        var kayttajanimi = document.forms["rekLomake"]["kayttajanimi"].value;
        var salasana = document.forms["rekLomake"]["salasana"].value;
        
        if(etunimi==null || etunimi=="",sukunimi==null || sukunimi=="", puhelinnumero==null || puhelinnumero=="",
        kotiosoite==null || kotiosoite=="",postinumero==null || postinumero=="",kaupunki==null || kaupunki=="",
        sahkoposti==null || sahkoposti=="",kayttajanimi==null || kayttajanimi=="",salasana==null || salasana==""){
            var errorMessage = "Lomakkeen kaikki kohdat tulee täyttää!";
            displayResult(errorMessage);
            return false;
        }
        return true;
    }
/*
* Tarkistetaan, että salasanat ovat yksi ja sama
*/
function tarkistaSalasana(){
    
    var salasana = document.getElementById('salasana');
    var salasananTarkistus = document.getElementById('salasananTarkistus');  
    
    if(salasana.value != salasananTarkistus.value){
        salasananTarkistus.setCustomValidity("Salasanat eivät ole sama!");
    } else{
        salasananTarkistus.setCustomValidity("");
    }
}


     
/*
* Tarkastetaan lomakkeen kunto/tila
*/
document.getElementById('submit').onclick = function(){
    tarkistaLomake();
}

/*
* Kokoaan asiakasobjektin lomakkeen input arvoista, ja tekee tästä
* JSON-objektin
*/
function kokoaAsiakas(){
    var asiakas = new Object();
        asiakas.etunimi = document.getElementById('etunimi').value;
        asiakas.sukunimi = document.getElementById('sukunimi').value;
        asiakas.puhelinnumero = document.getElementById('puhelinnumero').value;
        asiakas.kotiosoite = document.getElementById('kotiosoite').value;
        asiakas.postinumero = document.getElementById('postinumero').value;
        asiakas.kaupunki = document.getElementById('kaupunki').value;
        asiakas.sahkoposti = document.getElementById('sahkoposti').value;
        asiakas.kayttajanimi = document.getElementById('kayttajanimi').value;
        asiakas.salasana = document.getElementById('salasana').value;
       
    var jsonAsiakas = JSON.stringify(asiakas);
        
    lisaaAsiakas(jsonAsiakas);
}

/*
* Ajax POST-pyyntö palvelin REST api:lle
*/
function lisaaAsiakas(asiakas){
    var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
    xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
       // Typical action to be performed when the document is ready:
       var greatSuccess = "Asiakas lisätty onnistuneesti";
       displayResult(greatSuccess);
    }
};
    xmlhttp.open("POST", "https://verkkokauppa-joelhaa.c9users.io/public/index.php/api/asiakas/lisaa", true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send(asiakas);
};


/*
* Yksinkertainen alert funktio
*/
function displayResult(result){
    alert(result);
}

}