/*
* Ajax GET-pyyntö palvelin REST api:lle (pyytää kaikki asiakkaat)
*/
function haeKaikkiAsiakkaat(){
    var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
    xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
       // Typical action to be performed when the document is ready:
       document.getElementById('asiakaslista').innerHTML = xmlhttp.responseText;
    }
};
    xmlhttp.open("GET", "https://verkkokauppa-joelhaa.c9users.io/public/index.php/api/asiakkaat", true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send();
};
/*
* Ajax DELETE-pyyntö palvelin REST api:lle (poistaa annettun ID:n asiakkaan)
*/
function poistaAsiakas(){
    var asiakasID = document.getElementById('asiakasID').value;
    var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
    xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
       // Typical action to be performed when the document is ready:
       document.getElementById('asiakasPoistoTulos').innerHTML = xmlhttp.response;
    }
};
    xmlhttp.open("DELETE", "https://verkkokauppa-joelhaa.c9users.io/public/index.php/api/asiakas/poista/"+asiakasID, true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send();
};
/*
* Ajax GET-pyyntö palvelin REST api:lle (hakee kaikki tuotteet)
*/

function haeKaikkiTuotteet(){
    var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
    xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
       // Typical action to be performed when the document is ready:
       document.getElementById('tuotelista').innerHTML = xmlhttp.responseText;
    }
};
    xmlhttp.open("GET", "https://verkkokauppa-joelhaa.c9users.io/public/index.php/api/tuotteet", true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send();
};