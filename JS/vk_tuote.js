/*
* Funktio tuotteen hakemiseen
*/
function haeTuote(id){
    var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
    xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
       // Typical action to be performed when the document is ready:
       console.log("Tuote haettu onnistuneesti");
    }
};
    xmlhttp.open("GET", "https://verkkokauppa-joelhaa.c9users.io/public/index.php/api/tuote/"+id, true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send();
};
/*
* Funktio tuotearvosteluiden hakemiseen
*/
function haeTuoteArvostelut(){
    var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
    xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
       console.log("Tuotearvostelut haettu onnistuneesti.");
    }
};
    xmlhttp.open("GET", "https://verkkokauppa-joelhaa.c9users.io/public/index.php/api/tuotearvostelut", true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send();
}

/*
* Yksinkertainen alert funktio
*/
function displayResult(result){
    alert(result);
}