<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require '../src/config/db.php';


$app = new \Slim\App;

// Asiakkaat
require '../src/routes/asiakkaat.php';
// Tuotteet
//require '../src/routes/tuotteet.php';
// Tuotearvostelut
//require '../src/routes/tuotearvostelut.php';

$app->run();

